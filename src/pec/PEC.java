package pec;

/**
 * Interface Pending Event Container(PEC). This Interface sets the
 * guidelines to implement a PEC.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public interface PEC {
		/**
		 * Gets an Event from the PEC.
		 * @return Event The lowest ranking event from the PEC.
		 * @return null if the set is empty.
		 */
		public Event getEvent();
		/**
		 * Adds an Event to the PEC.
		 * @param event
		 */
		public void addEvent(Event event); 
}
