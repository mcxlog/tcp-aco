package pec;

import java.util.PriorityQueue;
/**
 * Implementation of the Pending Event Container(PEC) Interface with a 
 * priority queue to store the Events.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class PQPEC implements PEC {
	/**
	 * Priority queue of Events.
	 */
	protected PriorityQueue<Event> eventSet;
	
	/**
	 * Constructor for the PQPEQ class. Instanciates
	 * a new Priority queue.
	 */
	public PQPEC() {
		 eventSet = new PriorityQueue<Event>();
	}
 
	/*
	 * (non-Javadoc)
	 * @see pec.PEC#getEvent()
	 */
	@Override
	public Event getEvent(){
		Event e = eventSet.poll();
		return e;
	}
	
	/*
	 * (non-Javadoc)
	 * @see pec.PEC#addEvent(pec.Event)
	 */
	@Override
	public void addEvent(Event event){
		eventSet.add(event);
	}
	
}

