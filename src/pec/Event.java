package pec;

import java.util.LinkedList;
/**
 * The Event Interface provides the guidelines to implement event classes
 * to be handled by the Pending Event Container (PEC) Interface Implementations. 
 * In particular, a getter and setter for a Float timestamp variable must be
 * implemented, as well as a function to process the event when this is retrieved
 * from the PEC.
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public interface Event extends Comparable<Event>{
	
	/**
	 * Getter for the timestamp variable.
	 * @return Float
	 */
	public Float getTimestamp();
	
	/**
	 * Setter for the timestamp variable
	 * @param time value to set the timestamp
	 */
	public void setTimestamp(Float time);
	
	/**
	 * Function that processes the Event. The Exception thrown may
	 * change, depending on the implementation. A LinkedList of 
	 * arbitrary type elements is returned.  
	 * @return LinkedList<?>
	 * @throws Exception
	 */
	public LinkedList<?> processEvent() throws Exception;
}
