package tspAco;

/**
 * Exception thrown when the last observation is processed. 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class FinishException extends Exception {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for the FinishException Exception.
	 * @param string String to be passed to the super constructor.
	 */
	public FinishException(String string) {
		super(string);
	}
	
}
