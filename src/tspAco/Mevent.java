package tspAco;

import graph.Edge;
import graph.Graph;
import java.util.LinkedList;

/**
 * Concrete extension of the ACOEvent Abstract class to represent Move Events(Mevents) in
 * Ant Colony Optimization(ACO) applied to the Travelling Salesman Problem(TSP).
 * Each Move Event is associated with exactely one ant (and vice-versa). The Move events
 * are responsible for calling their ant's move() method, upon being removed from the PEC
 * and processed via processEvent();
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class Mevent extends ACOEvent {
	/**
	 * Simulation Parameters.
	 */
	static Float alpha,beta,delta,gama;
	/**
	 * Ant object associated with the Move event.
	 */
	private Ant ant;
	
	/**
	 * Constructor for creating a new Mevent. Must receive the graph
	 * in which the associated ant is traversing and a timestamp.
	 * @param timestamp timestamp of the Mevent
	 * @param graph Graph in which the associated ant is traversing
	 */
	public Mevent(Float timestamp, Graph<Integer,Edge<Float>> graph){
		this.setTimestamp(timestamp);
		this.ant = new Ant(graph, this);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Mevent["+ this.getTimestamp() +"]";
	}

	/*
	 * (non-Javadoc)
	 * @see tspAco.ACOEvent#processEvent()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public LinkedList<ACOEvent> processEvent() throws Exception{
		LinkedList<ACOEvent> returnList = new LinkedList<ACOEvent>();
		LinkedList<Edge<Float>> hamcycle;
		Float cycleWeight = 0.0f;
		ACOEvent.numberOfMevents++;// Increment the total number of triggered Mevents
		
		/*Check if the last move lead to an Hamiltonian Cycle and generate the corresponding Eevents if true.*/
		if((hamcycle = ant.checkHamiltoniancycle())!=null){
			for(Edge<Float> currentEdge: hamcycle){
				cycleWeight += currentEdge.getWeight();//Compute the weight of the cycle
			}
			/*Compute new pheromone levels*/
			Float newPheromoneLvl = (gama*(ant.getGraph().getTotalWeight()/2))/((float)(cycleWeight));
			
			for(Edge<Float> currentEdge: hamcycle){//For Each Edge in the Hamiltonian cycle
				/*Compute the next timestamp of the edge's Evaporation Event using an exponential distribution with mean value
				 *equal to the parameter eta.*/
				Eevent e=null;
			    if(currentEdge.getData().equals(0.0f))
			    	e = new Eevent(this.getTimestamp()+random.nextExpFloat(Eevent.eta),currentEdge);

			    
			    currentEdge.setData(currentEdge.getData()+ newPheromoneLvl);//Add the new pheromone level to the old one.
			    /*Compute the next timestamp of the edge's Evaporation Event using an exponential distribution with mean value
			     *equal to the parameter eta.*/
			    if(e!=null)
			    	returnList.add(e);//Add the newly created Evaporation Events(Eevents) to the return list
			}
			/*Updates the HamiltonianCycle (if the new path found is better than the previous one).*/
			if(cycleWeight<=ACOEvent.HamiltonianCycleWeight || ACOEvent.HamiltonianCycleWeight.equals(0.0f)){
					/* Set the new Hamiltonian cycle */
				ACOEvent.HamiltonianCycle = (LinkedList<Edge<Float>>) hamcycle.clone();//Suppressed Warning
				ACOEvent.HamiltonianCycleWeight = cycleWeight;
			}
			/*Resets the Ant*/
			ant.resetAnt();
		}
		
		/*Ant movement is performed*/
		this.ant.move();
		/*Add this Mevent to the PEC. The new timestamp has been computed inside the ant object associated with this event.*/
		returnList.add(this);
		
		return returnList;//Return all newly generated Eevents and this Mevent.
	}
}
