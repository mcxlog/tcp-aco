package tspAco;

import graph.Edge;
import java.util.LinkedList;
import pec.Event;

/**
 * Abstract Implementation of the Event Interface to be used in
 * Ant Colony Optimization(ACO) algorithms. This class offers 
 * method implementations for the getters and setters of the timestamp, as 
 * well as the compareTo method, essential to compare the events by their
 * timestamps when ordering them in the PEC. This class also specifies that the
 * return list of the process event method is composed by ACOEvent elements.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public abstract class ACOEvent implements Event{
	/**
	 * timestamp of the event.
	 */
	private Float timestamp;
	/**
	 * Total number of Eevents triggered so far.
	 */
	protected static Integer numberOfEevents = 0;
	/**
	 * Total number of Mevents triggered so far.
	 */
	protected static Integer numberOfMevents = 0;
	/**
	 * Hamiltonian cycle found so far.
	 */
	protected static LinkedList<Edge<Float>> HamiltonianCycle = null;
	/**
	 * Hamiltonian cycle's weight found so far.
	 */
	protected static Float HamiltonianCycleWeight = 0.0f;
	/**
	 * Random generator.
	 */
	protected static RandomUtil random = new RandomUtil();

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Event arg0) {
		if(this.timestamp<arg0.getTimestamp())
			return -1;
		else if(this.timestamp>arg0.getTimestamp())
			return 1;
		else
			return 0;
	}
	/*
	 * (non-Javadoc)
	 * @see pec.Event#getTimestamp()
	 */
	@Override
	public Float getTimestamp() {
		return timestamp;
	}

	/*
	 * (non-Javadoc)
	 * @see pec.Event#setTimestamp(java.lang.Float)
	 */
	@Override
	public void setTimestamp(Float time) {
		this.timestamp=time;

	}

	/*
	 * (non-Javadoc)
	 * @see pec.Event#processEvent()
	 */
	@Override
	public abstract LinkedList<ACOEvent> processEvent() throws Exception;

}
