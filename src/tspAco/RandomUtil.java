package tspAco;

import java.util.Random;

/**
 * Utilitary Class that extends the class Random, providing a method that
 * returns a random Float number with exponential distribution and mean
 * given by argument.
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class RandomUtil extends Random{
	private static final long serialVersionUID = 1L;

	/**
	 * Method that generates random Floats with exponential distribution with a given mean.
	 * @param mean mean of the exponential distribution.
	 * @return Float
	 */
	public Float nextExpFloat(Float mean){
		return new Float(-mean*(float)Math.log(1-this.nextFloat()));
	}

}
