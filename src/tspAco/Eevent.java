package tspAco;

import graph.Edge;
import java.util.LinkedList;

/**
 * Concrete extension of the ACOEvent Abstract class to represent Evaporation Events(Eevents) in
 * Ant Colony Optimization(ACO) applied to the Travelling Salesman Problem(TSP).
 * Each Evaporation Event is associated with exactely one Edge. The Evaporation events
 * are responsible for updating the Pheromone level of the Edges.
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class Eevent extends ACOEvent{
	/**
	 * Simulation Parameter related to the probability exponential distribution
	 * that rules Pheromone placement.
	 */
	static Float eta;
	/**
	 * Simulation Parameter that defines the quantity that the Pheromone levels are
	 * decreased each time an Eevent is triggered.
	 */
	static Float rho; 
	/**
	 * Edge associated with the Eevent.
	 */
	private Edge<Float> edge;
	
	/**
	 * Constructor for creating a new Eevent. Must receive an Edge
	 * to associate with and a timestamp.
	 * @param timestamp timestamp of the Mevent.
	 * @param edge Edge to associate the Eevent with.
	 */
	public Eevent(Float timestamp, Edge<Float> edge) {
		this.setTimestamp(timestamp);
		this.edge = edge;
	}
	
	/*
	 * (non-Javadoc)
	 * @see tspAco.ACOEvent#processEvent()
	 */
	@Override
	public LinkedList<ACOEvent> processEvent(){
		LinkedList<ACOEvent> returnList = new LinkedList<ACOEvent>();
		Float updtPheromoneLvl;
		
		ACOEvent.numberOfEevents++;//Increment the total number of triggered Eevents
	
		/*If next level of pheromone is positive, update it and return the Eevent with new timestamp*/
		if((updtPheromoneLvl = this.edge.getData()-rho)>0.0f){
			this.edge.setData(updtPheromoneLvl);//Update the Pheromone levels
			/*Compute the next timestamp of the edge's Evaporation Event using an exponential distribution 
			 *with mean value equal to the parameter eta.*/
			this.setTimestamp(this.getTimestamp()+random.nextExpFloat(Eevent.eta));
			returnList.add(this);//add the Eevent to the return list
		}else{//The Pheromones have evaporated completely
			this.edge.setData(0.0f);//Set the pheromone level of the Edge to 0.
		}
		return returnList;
	}

}
