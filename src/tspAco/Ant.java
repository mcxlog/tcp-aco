package tspAco;

import java.util.LinkedList;
import graph.*;

/**
 * The Ant class represents the ants that traverse the graph, searching
 * for hamiltonian cycles. The ant move() method is called by the Mevent
 * associated with the ant, when the event is collected from the PEC and processed.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class Ant{
	private static RandomUtil random = new RandomUtil();
	/**
	 * Graph in which the Ant will travel.
	 */
	private Graph<Integer, Edge<Float>> graph;
	/**
	 * Move event related to the Ant.
	 */
	private Mevent mevent; 
	/**
	 * Node where the Ant currently is.
	 */
	private Integer currentNode;
	/**
	 * Path Traversed so far by the Ant.
	 */
	private LinkedList<Edge<Float>> currentPath;

	/**
	 * This constructor binds the Ant to a graph and to 
	 * a Move Event.
	 * @param graph Graph in which the ant travel. 
	 * @param mevent  Move Event associated with the ant.
	 */
	public Ant(Graph<Integer, Edge<Float>> graph, Mevent mevent){
		this.graph = graph;
		this.currentNode = graph.getOriginNode();
		this.mevent = mevent; 
		this.currentPath = new LinkedList<Edge<Float>>();
	}
	/**
	 * Returns the graph in which the ant is traveling
	 * @return graph
	 */
	public Graph<Integer, Edge<Float>> getGraph() {
		return graph;
	}

	/**
	 * Check if the ant has found an Hamiltonian cycle.
	 * @return currentPath if the current is an HamiltonianCycle 
	 * @return null if the current is not an HamiltonianCycle 
	 */
	public LinkedList<Edge<Float>> checkHamiltoniancycle(){ 
		if(!this.currentPath.isEmpty()){
			if(this.currentPath.size()==this.graph.getNumberOfNodes()){
				return this.currentPath;
			}
		}
		return null;
	}
	/**
	 * Reset the ant's current path and place the ant at 
	 * the origin node of the graph. This is done every time the ant
	 * finds an Hamiltonian cycle.
	 */
	public void resetAnt(){
		this.currentPath.removeAll(this.currentPath);
		this.currentNode = graph.getOriginNode();
	}
	/**
	 * Move method. This method is called by the Mevent associated with the ant. In this method,
	 * the ant selects the next edge to travel, and if a cycle is found, it removes the cycle from
	 * it's path. The new timestamp for the associated Mevent is computed and set here, but the computation and
	 * placing of pheromones is done in the Mevent's processEvent() method. 
	 * @throws Exception Exception thrown if the adjacency of a node is empty.
	 */
	public void move() throws InvalidGraphException{
		float tmpsum = 0.0f;
		float rand = random.nextFloat();//Get a random Float
		LinkedList<Edge<Float>> adjList = graph.getEdges(currentNode);//Get the adjacency edge list of the current node 
		LinkedList<Edge<Float>> nonvisitedEdges = new LinkedList<Edge<Float>>(); //Edges which lead to nodes not yet visited
		
		if(adjList==null){//This can only happen if the nestnode does not have any Neighbours. This is not a practical graph to perform ACO TSP.
			throw(new InvalidGraphException("Empty Adjacency List for the node "+currentNode));
		}
		
		/* Fill the nonvisited edges list */
		for(Edge<Float> edge: adjList){//For every edge in the adjacency list of the current node
			if(this.currentPath.indexOf(edge)==-1 ){//If the edge isn't already in the currentPath
				/*Only include an edge pointing to the origin node to close Hamiltonian cycles.*/
				if((!edge.getV().equals(this.graph.getOriginNode())) || graph.getNumberOfNodes().equals(this.currentPath.size()+1))
					nonvisitedEdges.add(edge);//Add nonvisited edges to the list
			}
		}
		
		/*For all non-visited Edges, compute the probability of choosing them as the next edge to visit,
		 *and then make the choice.*/
		if(!nonvisitedEdges.isEmpty()){//if there are non-visited edges
			Float cij[] = new Float[nonvisitedEdges.size()];
			float sum = 0.0f;
			int i=0;
			for(Edge<Float> edge: nonvisitedEdges){//For all non visited edges
					/*Compute the edge's coefficient*/
				cij[i]=(Mevent.alpha + edge.getData())/(Mevent.beta + edge.getWeight());
					/*Accumulate the edge's coefficient. In the end of the cycle, <sum> contains the sum of all coefficients*/
				sum+=cij[i].floatValue();
				i++;
			}
			
			for(i=0;i<nonvisitedEdges.size();i++){//For all non visited edges
				/*The edges are chosen taken into account their probabilities <cij[i]/sum>*/
				tmpsum+=cij[i]/sum;//Compute probability interval. <tmpsum> will be 1 if this cycle reaches the last Edge.
				if(rand<=tmpsum){//Edge has been chosen
					Edge<Float> chosenEdge = nonvisitedEdges.get(i);
					this.currentNode = chosenEdge.getV();//Set the current node as the opposite node of the edge.
					this.currentPath.addLast(chosenEdge);//Add the Edge to the path
						/*Compute the next time stamp of the ant's Move Event using an exponential distribution with mean value
						 *equal to the weight of the edge times the delta parameter.*/
					this.mevent.setTimestamp(this.mevent.getTimestamp()+random.nextExpFloat(chosenEdge.getWeight()*Mevent.delta));
					return;
				}
			}
		}
		/*A cycle has been found: Remove the cycle*/
		for(int i=0;i<adjList.size();i++){
			if(rand<=((float)i)/((float)adjList.size())){//Chose an adjacent Edge with random uniform probability distribution
				Edge<Float> chosenEdge = adjList.get(i);
				this.currentNode = chosenEdge.getV();
				/*Traverse the cycle and remove the edges*/
				while(!this.currentPath.getLast().getV().equals(this.currentNode)){
					this.currentPath.removeLast();
					if(this.currentPath.isEmpty()){
						break;
					}
				}
				/*Compute the next time stamp of the ant's Move Event using an exponential distribution with mean value
				 *equal to the weight of the edge times the delta parameter.*/
				this.mevent.setTimestamp(this.mevent.getTimestamp()+random.nextExpFloat(chosenEdge.getWeight()*Mevent.delta));
				return;
			}
		}
		return;
	}
}
