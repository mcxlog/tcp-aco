package tspAco;

import graph.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * Parser for xml files compliant with simulation.dtd. This parser
 * reads the xml file provided as an argument in the constructor,
 * fills a graph(also provided) and sets the simulation parameters
 * also read from the xml file.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class SimParser extends DefaultHandler{
	
	/**
	 * Graph provided as input
	 */
	private AdjListGraph<Float> graph;
	/**
	 * Auxiliary variables to insert edges in the graph
	 */
	private Integer currentNode, targetNode;
	/**
	 * Auxiliary StringBuffer to hold character data read
	 * from the xml file.
	 */
	private StringBuffer charData;
	/**
	 * The SAX parser
	 */
	private SAXParser parser;
	
	/**
	 * Input file
	 */
	private File file;
	
	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String name,String tag, Attributes atts) throws SAXException{
		
		/*Clear the charData buffer to collect data from a new field*/
		charData.setLength(0);
		
		if(tag.equals("weight")){/*Beginning of Edge definition: Set target node*/
			targetNode=Integer.decode(atts.getValue("targetnode"));
			
		}else if(tag.equals("node")){/*Beginning of Node definition: Set current node*/
			currentNode=Integer.decode(atts.getValue("nodeidx"));
			
		}else if(tag.equals("graph")){/*Beginning of Graph definition: Set graph parameters*/
			graph.setNumberOfNodes(Integer.decode(atts.getValue("nbnodes")));
			graph.setOriginNode(Integer.decode(atts.getValue("nestnode")));
			if(graph.getOriginNode()>graph.getNumberOfNodes() || graph.getOriginNode()<1){
				throw new SAXException("The origin node must be inside the graph (within the range [1,nbnodes]).");
			}
			
		}else if(tag.equals("move")){/*Get Move Input Parameters*/
			Mevent.alpha=Float.parseFloat(atts.getValue("alpha"));
			Mevent.beta=Float.parseFloat(atts.getValue("beta"));
			Mevent.delta=Float.parseFloat(atts.getValue("delta"));
			
		}else if(tag.equals("evaporation")){/*Get Evaporation Input Parameters*/
			Eevent.eta=Float.parseFloat(atts.getValue("eta"));
			Eevent.rho=Float.parseFloat(atts.getValue("rho"));
			
		}else if(tag.equals("simulation")){/*Get Simulation Input Parameters*/
			Observation.tau = Float.parseFloat((atts.getValue("finalinst")));
			TspAcoSimulator.antColSize=Integer.decode(atts.getValue("antcolsize"));
			Mevent.gama=Float.parseFloat(atts.getValue("plevel"));
			if(Observation.tau <= 0.0f)
				throw new SAXException("The simulation final instant must be a non-zero positive number.");
			if(TspAcoSimulator.antColSize<=0)
				throw new SAXException("The size of the colony must be a non-zero positive number.");
			
		}
	}
	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String name,String tag) throws SAXException{
		if(tag.equals("weight")){/*End of Edge definition: Insert Edge*/
			float weight = Float.parseFloat(charData.toString());
			boolean duplicateEdge=false;
			
			/*Insert Edge only if it does not already exist.*/
			LinkedList<Edge<Float>> edges;
			if((edges=graph.getEdges(currentNode))!=null){
				for(Edge<Float> currentedge : edges){
					if(currentedge.getV().equals(targetNode)){
						/*Found duplicate edge. If the weight is equal to 
						 *to one trying to be inserted, ignore. If it is different,
						 *throw an exception to abort the program's execution.*/
						if(currentedge.getWeight().equals(new Float(weight))){
							duplicateEdge=true;
						}else{
							throw new SAXException("Found duplicate edge with a different weight! Aborted.");
						}
					}
				}
			}
			
			if(!duplicateEdge){//if duplicateEdge==true, ignore it.
				Edge<Float> edge = new Edge<Float>(currentNode,targetNode,weight,new Float(0));
				Edge<Float> invedge = new Edge<Float>(targetNode,currentNode,weight,new Float(0));
				graph.insertEdge(edge);
				graph.insertEdge(invedge);
			}
			
		}
	}
	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	@Override
	public void characters(char[]ch,int start,int length){
		charData.append(new String(ch,start,length));/*Collect data from the current field*/
	}
	/**
	 * Constructor for the SimParser Class. The SimParser reads an xml File and
	 * fills the provided graph object.
	 * @param graph Input graph object to be filled with data from the xml file.
	 * @param file Opened xml file.
	 * @throws ParserConfigurationException Exceptions thrown during the configuration of the parser.
	 * @throws SAXException Exceptions thrown during the parsing of the xml file or creation of the parser.
	 */
	public SimParser(AdjListGraph<Float> graph, File file) throws ParserConfigurationException, SAXException{
		this.file = file;
		this.graph = graph;
		this.charData = new StringBuffer();
			//builds the SAX parser
		SAXParserFactory fact = SAXParserFactory.newInstance();
		fact.setValidating(true);
		this.parser = fact.newSAXParser();
	}
	/**
	 * Parsing method of the SimParser. This method sets the simulation variables and fills the
	 * graph provided in the constructor.
	 * @throws SAXException Exceptions thrown during the parsing of the xml file.
	 * @throws IOException Exceptions thrown in case of file IO error.
	 */
	public void parse() throws SAXException, IOException{
		this.parser.parse(file, this);
	}
	
}
