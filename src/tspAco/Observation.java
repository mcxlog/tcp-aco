package tspAco;

import graph.Edge;

import java.util.LinkedList;

/**
 * 
 * Concrete extension of the ACOEvent Abstract class to represent Observation Events in
 * Ant Colony Optimization(ACO) applied to the Travelling Salesman Problem(TSP). This Event
 * reports the current state of the simulation to the output console. In practice, only one of
 * this Events is instantiated and triggered 20 times, every tau/20 time units.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class Observation extends ACOEvent{
	/**
	 * Total simulation running time.
	 */
	public static Float tau;
	
	/**
	 * Constructor for creating a new Observation. Sets the
	 * initial timestamp to tay/20.
	 */
	public Observation(){
		this.setTimestamp(tau/20);
	}
	/*
	 * (non-Javadoc)
	 * @see tspAco.ACOEvent#processEvent()
	 */
	@Override
	public LinkedList<ACOEvent> processEvent() throws FinishException{
		LinkedList<ACOEvent> returnList = new LinkedList<ACOEvent>();

		/*Print the current state of the simulation to the output console*/
		System.out.println("Observation " + (int)(this.getTimestamp()/(tau/20)) + ":");
		System.out.println("\t\t Present instant:\t\t"+this.getTimestamp());
		System.out.println("\t\t Number of move events:\t\t"+ACOEvent.numberOfMevents);
		System.out.println("\t\t Number of evaporation events:\t"+ACOEvent.numberOfEevents);
		System.out.print("\t\t Hamiltonian cycle:\t\t");
		
		/* Print the Hamiltonian cycle */
		if(ACOEvent.HamiltonianCycle!=null){
			System.out.print("{");
			int i=0;
			for(Edge<Float> currentEdge :ACOEvent.HamiltonianCycle){
				if(i!=0)System.out.print(",");
				System.out.print(currentEdge);
				i++;
			}
			System.out.println("}");
		}else{
			System.out.println("{}");
		}

		/* Check if the simulation has ended */
		if((int)this.getTimestamp().intValue()==(int)tau.intValue()) /*XXX This is the best way to compare those floats=?!*/
			throw new FinishException("Success");
		
		/*Set the next timestamp (current time stamp + tau/20)*/
		this.setTimestamp(this.getTimestamp()+tau/20);
		returnList.add(this);//Add the Event to the return list
		return returnList;
	}

}
