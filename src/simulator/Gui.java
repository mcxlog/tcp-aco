package simulator;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

/**
 * Graphical Interface and graph generator class.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class Gui implements Runnable{

	/*Create the main frame*/
	private static JFrame frame = new JFrame("Tsp Aco Simulator");
	
	private static File fileptr=null;

	public Gui() {
		super();
	}

	@Override
	public void run() {

		/*Menubars*/
		JMenuBar menuBar;
		JMenu menu;
		menuBar = new JMenuBar();
		menu = new JMenu("Menu");
		menuBar.add(menu);
		JMenuItem aboutItem = new JMenuItem("About");
		menu.add(aboutItem);
		
		/*file chooser to open files*/
		JFileChooser fileChoser = new JFileChooser();
		fileChoser.setFileFilter(new FileNameExtensionFilter(".xml", "xml"));

		/*Create a Container Panel and sub panels*/
		JPanel textPanel = new JPanel(new BorderLayout(10,10));
		JPanel controlPanel = new JPanel(new GridLayout());
		GridLayout genLayout = new GridLayout(6,1);
		JPanel genPanel = new JPanel(genLayout);
		JPanel simPanel = new JPanel(new FlowLayout());

		/*Create a Text Pane and a Scroll pane*/
		JTextPane gText = new JTextPane();
		JScrollPane ScrollConsole = new JScrollPane(gText);
		ScrollConsole.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		ScrollConsole.setPreferredSize(new Dimension(500, 100));
		TitledBorder border0 = BorderFactory.createTitledBorder("Console");
		border0.setTitleJustification(TitledBorder.CENTER);	
		ScrollConsole.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createCompoundBorder(border0,
						BorderFactory.createEmptyBorder(5,5,5,5)),
						ScrollConsole.getBorder()));
		ScrollConsole.setPreferredSize(new Dimension(600,250));
		ScrollConsole.setAutoscrolls(false);
		
		/*Redirect the printing Stream to the gText*/
		ForkedStream gStream = new ForkedStream(gText);
		System.setOut(new PrintStream(gStream, true));
		System.setErr(new PrintStream(gStream, true));

		/*Create a button, checkbox and setup the control panel*/
		JButton btn = new JButton("Load and simulate");
		JCheckBox cleanCheck = new JCheckBox("clear previous output");
		btn.setSize(100, 10);
		TitledBorder border = BorderFactory.createTitledBorder("Control Panel");
		border.setTitleJustification(TitledBorder.CENTER);	
		controlPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createCompoundBorder(border,
						BorderFactory.createEmptyBorder(5,5,5,5)),
						controlPanel.getBorder()));
		
		/*Create other components for the gen panel*/
		JButton btnGen = new JButton("Generate xml");

		GenActionListener.labelNodes.setText("nodes:");
		GenActionListener.txtNodes.setText("10");

		GenActionListener.labelNest.setText("nest node:");
		GenActionListener.txtNest.setText("1");

		GenActionListener.labelInst.setText("final inst:");
		GenActionListener.txtInst.setText("300.0");

		GenActionListener.labelCol.setText("ants:");
		GenActionListener.txtCol.setText("200");

		GenActionListener.labelGama.setText("gama:");
		GenActionListener.txtGama.setText("0.5");

		GenActionListener.labelAlpha.setText("alpha:");
		GenActionListener.txtAlpha.setText("1.0");

		GenActionListener.labelBeta.setText("beta:");
		GenActionListener.txtBeta.setText("1.0");

		GenActionListener.labelDelta.setText("delta:");
		GenActionListener.txtDelta.setText("0.2");

		GenActionListener.labelEta.setText("eta:");
		GenActionListener.txtEta.setText("2.0");

		GenActionListener.labelRho.setText("rho:");
		GenActionListener.txtRho.setText("10.0");

		GenActionListener.labelProb.setText("link prob:");
		GenActionListener.txtProb.setText("0.5");
		
		genPanel.add(GenActionListener.labelNodes);
		genPanel.add(GenActionListener.txtNodes);

		genPanel.add(GenActionListener.labelNest);
		genPanel.add(GenActionListener.txtNest);

		genPanel.add(GenActionListener.labelInst);
		genPanel.add(GenActionListener.txtInst);

		genPanel.add(GenActionListener.labelCol);
		genPanel.add(GenActionListener.txtCol);

		genPanel.add(GenActionListener.labelGama);
		genPanel.add(GenActionListener.txtGama);

		genPanel.add(GenActionListener.labelAlpha);
		genPanel.add(GenActionListener.txtAlpha);

		genPanel.add(GenActionListener.labelBeta);
		genPanel.add(GenActionListener.txtBeta);

		genPanel.add(GenActionListener.labelDelta);
		genPanel.add(GenActionListener.txtDelta);

		genPanel.add(GenActionListener.labelEta);
		genPanel.add(GenActionListener.txtEta);

		genPanel.add(GenActionListener.labelRho);
		genPanel.add(GenActionListener.txtRho);
		
		genPanel.add(GenActionListener.labelProb);
		genPanel.add(GenActionListener.txtProb);

		/*Create some sub panels*/
		JPanel genPanelBottom = new JPanel();
		JCheckBox GuaranteeCycle = new JCheckBox("Guarantee Hamiltonian Cycle");
		genPanelBottom.add(GuaranteeCycle);
		genPanelBottom.add(btnGen);
		JSplitPane splitGen = new JSplitPane(JSplitPane.VERTICAL_SPLIT,genPanel,genPanelBottom);
		JPanel genPanelRule = new JPanel(new GridLayout());
		TitledBorder border2 = BorderFactory.createTitledBorder("Xml Generation Panel");
		border2.setTitleJustification(TitledBorder.CENTER);	
		genPanelRule.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createCompoundBorder(border2,
						BorderFactory.createEmptyBorder(5,5,5,5)),
						genPanel.getBorder()));
		genPanelRule.add(splitGen);

		/*Simulation Panel*/
		simPanel.add(btn);
		simPanel.add(cleanCheck);

		TitledBorder border3 = BorderFactory.createTitledBorder("Simulation Panel");
		border3.setTitleJustification(TitledBorder.CENTER);	
		simPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createCompoundBorder(border3,
						BorderFactory.createEmptyBorder(5,5,5,5)),
						simPanel.getBorder()));

		/*Build the gui*/
		controlPanel.add(simPanel);
		controlPanel.add(genPanelRule);
		textPanel.add(ScrollConsole);

		JSplitPane splitpane0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT,textPanel,controlPanel);

		frame.add(splitpane0);			
		frame.setJMenuBar(menuBar);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
		frame.setVisible(true);

		/*Add action listeners to the buttons*/
		btn.addActionListener(new GoActionListener(cleanCheck,gText,fileChoser));
		aboutItem.addActionListener(new aboutActionListener());
		btnGen.addActionListener(new GenActionListener(GuaranteeCycle));

	}
	private static class aboutActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(frame,
				    "Graphical user interface for Traveling Salesman Problem\n" +
				    "with Ant Colony Optimization simulation.\n" +
				    "Object Oriented Programming course, Instituto Superior Tecnico.\n\n" +
				    "Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com\n"+
					"Tomas Ferreirinha, 67725. tomas619@hotmail.com");
		}
	}
	private static class GenActionListener implements ActionListener{

		private final static	JLabel labelNodes = new JLabel();
		private final static	JTextField txtNodes = new JTextField();

		private final static	JLabel labelNest = new JLabel();
		private final static	JTextField txtNest = new JTextField();

		private final static	JLabel labelInst = new JLabel();
		private final static	JTextField txtInst = new JTextField();

		private final static	JLabel labelCol = new JLabel();
		private final static	JTextField txtCol = new JTextField();

		private final static	JLabel labelGama = new JLabel();
		private final static	JTextField txtGama = new JTextField();

		private final static	JLabel labelAlpha = new JLabel();
		private final static	JTextField txtAlpha = new JTextField();

		private final static	JLabel labelBeta = new JLabel();
		private final static	JTextField txtBeta = new JTextField();

		private final static	JLabel labelDelta = new JLabel();
		private final static	JTextField txtDelta = new JTextField();

		private final static	JLabel labelEta = new JLabel();
		private final static	JTextField txtEta = new JTextField();

		private final static	JLabel labelRho = new JLabel();
		private final static	JTextField txtRho = new JTextField();
		
		private final static	JLabel labelProb = new JLabel();
		private final static	JTextField txtProb = new JTextField();

		private static Random random = new Random();

		private JCheckBox gcycle; 
		public GenActionListener(JCheckBox GuaranteeCycle){
			this.gcycle = GuaranteeCycle;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			try {
				if(Integer.parseInt(GenActionListener.txtNodes.getText())>500){
					int retPane=JOptionPane.showConfirmDialog(
	                        frame, "You are attempting to generate a big graph(nodes>500)\n" +
	                        		"This operation may result in a memory error.\n" +
	                        		"Do you want to proceed?",
	                        "Big Graph Generation",
	                        JOptionPane.YES_NO_OPTION,
	                        JOptionPane.WARNING_MESSAGE);
					
					 if(retPane != JOptionPane.YES_OPTION){
						return; 
					 }
				}
                     
				
				frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			    
				/*DOM document writing*/
				
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				org.w3c.dom.Document doc = docBuilder.newDocument();
				doc.setXmlVersion("1.0");
				// root elements
				Element rootElement = doc.createElement("simulation");
				rootElement.setAttribute("finalinst",GenActionListener.txtInst.getText());
				rootElement.setAttribute("antcolsize", GenActionListener.txtCol.getText());
				rootElement.setAttribute("plevel", GenActionListener.txtGama.getText());
				doc.appendChild(rootElement);

				// Graph
				Element graph = doc.createElement("graph");
				graph.setAttribute("nbnodes",GenActionListener.txtNodes.getText());
				graph.setAttribute("nestnode",GenActionListener.txtNest.getText());
				rootElement.appendChild(graph);

				int nnodes=Integer.parseInt(GenActionListener.txtNodes.getText());		
				int targetNode,weightValue,firstj,lastj;
				float prob = Float.parseFloat(GenActionListener.txtProb.getText());
				boolean docycle=true;
				
				for(int i=0;i<nnodes;i++){
					Element node = doc.createElement("node");									
					node.setAttribute("nodeidx",Integer.toString(i+1));
					
					if(gcycle.isSelected()){//Guarantee cycle
						
						if(i!=(nnodes-1)){
							targetNode = i+1;
						}else{//last node ; loops
							targetNode = 0;
						}
						
						weightValue = random.nextInt(9)+1;
						Element weight = doc.createElement("weight");									
						weight.setAttribute("targetnode",Integer.toString(targetNode+1));
						weight.appendChild(doc.createTextNode(Integer.toString(weightValue)));
						node.appendChild(weight);			
					}
					if(i!=(nnodes-1)){
						
						if(gcycle.isSelected()){
							docycle=true;
							if(i==(nnodes-2))
							{
								firstj=nnodes;
								docycle=false;
							}else
								firstj=i+2;
							
							if(i==0){
								lastj=nnodes-1;
							}else
								lastj=nnodes;
							
						}else{//Cicle was NOT guaranteed
							firstj=i+1;
							lastj=nnodes;
						}
						if(docycle){							
							for(int j=firstj;j<lastj;j++){
								
								if(random.nextFloat()<prob){//probability of connection
								
									targetNode = j;
									weightValue = random.nextInt(9)+1;
									Element weight = doc.createElement("weight");									
									weight.setAttribute("targetnode",Integer.toString(targetNode+1));
									weight.appendChild(doc.createTextNode(Integer.toString(weightValue)));
									node.appendChild(weight);	
									
								}
							}
						}
					}
					graph.appendChild(node);
				}

				// 
				Element events = doc.createElement("events");
				rootElement.appendChild(events);

				Element move = doc.createElement("move");
				move.setAttribute("alpha",GenActionListener.txtAlpha.getText());
				move.setAttribute("beta",GenActionListener.txtBeta.getText());
				move.setAttribute("delta",GenActionListener.txtDelta.getText());
				events.appendChild(move);

				Element evaporation = doc.createElement("evaporation");
				evaporation.setAttribute("eta",GenActionListener.txtEta.getText());
				evaporation.setAttribute("rho",GenActionListener.txtRho.getText());
				events.appendChild(evaporation);

				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();

				transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "simulation.dtd");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

				frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				
				JFileChooser filesaver = new JFileChooser();
				filesaver.setFileFilter(new FileNameExtensionFilter(".xml", "xml"));
				filesaver.setApproveButtonText("Save");
				filesaver.setDialogTitle("Save");
				int option = filesaver.showOpenDialog(null);  
				if(option == JFileChooser.APPROVE_OPTION){  

					File fileToSave;
					File rawfile = filesaver.getSelectedFile();
					if(rawfile!=null){  
						if(!rawfile.getName().contains(".xml"))
							fileToSave = new File(rawfile + ".xml");
						else
							fileToSave = rawfile;

						DOMSource source = new DOMSource(doc);
						StreamResult result = new StreamResult(fileToSave);
						transformer.transform(source, result);
						JOptionPane.showMessageDialog(null,"Xml simulation file written: "+fileToSave.getAbsolutePath());
					}else
						JOptionPane.showMessageDialog(null,"No file Selected");
				}else
					JOptionPane.showMessageDialog(null,"Save operation canceled");

			} catch (ParserConfigurationException pce) {
				JOptionPane.showMessageDialog(null, "Unknown Error!");
				//pce.printStackTrace();
			} catch (TransformerException tfe) {
				JOptionPane.showMessageDialog(null, "File Error!");
				//tfe.printStackTrace();
			} catch (OutOfMemoryError E) {
				frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				JOptionPane.showMessageDialog(null, "Not enought memory! Aborting graph generation.");
				//e.printStackTrace();
			}
		}
	}
	private static class ForkedStream extends OutputStream{

		private JTextPane textpane; 

		public ForkedStream(JTextPane textpane){
			super();
			this.textpane=textpane;
		}
		@Override
		public void write(final int b) throws IOException {

			Document document = textpane.getDocument();
			try {
				document.insertString(document.getLength(),String.valueOf((char) b), null);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			textpane.setCaretPosition(document.getLength() - 1);
		}

		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			Document document = textpane.getDocument();
			try {
				document.insertString(document.getLength(),new String(b, off, len), null);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			textpane.setCaretPosition(document.getLength() - 1);
		}

		@Override
		public void write(byte[] b) throws IOException {
			write(b, 0, b.length);
		}
	}
	private static class GoActionListener implements ActionListener{

		private JCheckBox cleanCheck;
		private JTextPane textpane;
		private JFileChooser fileChoser;

		public GoActionListener(JCheckBox cleanCheck,JTextPane textpane,JFileChooser fileChoser){
			this.cleanCheck = cleanCheck;
			this.textpane=textpane;
			this.fileChoser=fileChoser;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {

			if(Simulator.tspAcoSimulator!=null){
				if(Simulator.tspAcoSimulator.isAlive()){
					JOptionPane.showMessageDialog(null,"Please wait for the current simulation to finish.");
					return;
				}
			}
			if(this.cleanCheck.isSelected()){
				this.textpane.setText("");
			}
			int returnVal = this.fileChoser.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				Gui.fileptr = this.fileChoser.getSelectedFile();
				if(!Gui.fileptr.exists()){
					System.out.println("Failed to open simulation file: "+Gui.fileptr.getAbsolutePath());
					Gui.fileptr=null;
				}else{
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					JOptionPane.showMessageDialog(null,"Will now simulate: "+Gui.fileptr.getAbsolutePath()+"."+" This may take a while.");
					Simulator.issueSimulationFromXmlFile(Gui.fileptr);
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

				}
			}else
				JOptionPane.showMessageDialog(null,"Please load an xml simulation file.");
		}
	}
}