package simulator;
import java.io.File;
import javax.swing.SwingUtilities;

import tspAco.TspAcoSimulator;

/**
 * This class is the main project class and is a wrapper of the TspAcoSimulator class and the Gui class.
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class Simulator{

	/**
	 * Sets if GUI is enabled or not, depending on program arguments.
	 */
	private static boolean enablegui = true;
	/**
	 * Traveling Salesman Problem with Ant Collony Optimization simulator.
	 */
	protected static TspAcoSimulator tspAcoSimulator;
	/**
	 * Main Method of the Simulator class. Receives an xml input
	 * file as args[0]. Is GUI is disabled, the xml parsing is issued to the SaxParser class, 
	 * and secondly a TspAcoSimlulator is created and ordered to start. Otherwise, the GUI
	 * is launched and operations are controlled by the user.*/
	public static void main(String args[]){

		if(args.length>=1){
			if(args[0].equals("-help")){
				System.out.println("[Usage] TspAcoSimulator [filename.xml]");
				System.out.println("filename.xml: specifies the simulation file. If provided, GUI is disabled.");			
				return;
			}
		}
		if(args.length>=1){//If no arguments are provided, enable gui.
			enablegui=false;
		}
		if(enablegui){
			SwingUtilities.invokeLater(new Gui());
		}else{
			File file = new File(args[0]);
			if(file!=null){
				if(file.exists()){
					issueSimulationFromXmlFile(file);//Issue the simulation
					return;
				}
			}
			System.out.println("Failed to open simulation file: "+file.getAbsolutePath());
			return;
		}
	}
	/**
	 * This method creates a TspAcoSimulator and issues a simulation loaded from an xml file.
	 * @param file xml simulation file.
	 */
	public static void issueSimulationFromXmlFile(File file){

		/*Create a new TSP ACO Simulator*/
		tspAcoSimulator = new TspAcoSimulator(file);
		/*Reset Static variables*/
		tspAcoSimulator.reset();
		/*Start simulation Thread*/
		tspAcoSimulator.start();
		return;
	}
}