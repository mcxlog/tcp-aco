package graph;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Generic class that implements a graph with an adjacency list
 * implemented with an HashMap that links 
 * a LinkedList of adjacent Edges for each Integer (Node). 
 * @param <D> Data type to be stored inside an Edge.
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class AdjListGraph<D> implements Graph<Integer,Edge<D>> {

	/**
	 * Number of nodes in the graph.
	 */
	private Integer numberOfNodes;
	/**
	 * Origin node of the graph.
	 */
	private Integer originNode;
	/**
	 * Sum of the weights of all the edges.
	 */
	private Float totalWeight;
	/**
	 * HashMap that links a LinkedList of adjacent Edges for each Integer (Node). 
	 */
	private HashMap<Integer, LinkedList<Edge<D>>> adjList;
	
	/**
	 * Constructor for this class. Instantiates the Adjacency list of the graph and
	 * sets the totalWeight to zero.
	 */
	public AdjListGraph() {
		this.adjList = new HashMap<Integer, LinkedList<Edge<D>>>();
		this.totalWeight=0.0f;
	}
	
	/*
	 * (non-Javadoc)
	 * @see graph.Graph#insertEdge(java.lang.Object)
	 */
	@Override
	public void insertEdge(Edge<D> edge){
		LinkedList<Edge<D>> linkedList;
		linkedList = adjList.get(edge.getU());
		if(linkedList!=null){
			linkedList.add(edge);//Add the edge to the node's list
		}else{//If the node does not yet have an Edge list
			linkedList = new LinkedList<Edge<D>>();//Create the edge list
			linkedList.add(edge);//Add the edge to the node's list
			adjList.put(edge.getU(),linkedList);//Add the list to the HashMap, keyed with the node.
		}
		this.totalWeight+=edge.getWeight();//Increment the total weight of the graph
	}

	/*
	 * (non-Javadoc)
	 * @see graph.Graph#getEdges(java.lang.Object)
	 */
	@Override
	public LinkedList<Edge<D>> getEdges(Integer u) {
		return adjList.get(u);
	}

	/*
	 * (non-Javadoc)
	 * @see graph.Graph#getOriginNode()
	 */
	@Override
	public Integer getOriginNode() {
		return this.originNode;
	}

	/**
	 * Sets the number of nodes.
	 * @param numberOfNodes
	 */
	public void setNumberOfNodes(Integer numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}
	/**
	 * Sets the origin node
	 * @param originNode
	 */
	public void setOriginNode(Integer originNode) {
		this.originNode = originNode;
	}
	/*
	 * (non-Javadoc)
	 * @see graph.Graph#getNumberOfNodes()
	 */
	@Override
	public Integer getNumberOfNodes() {
		// TODO Auto-generated method stub
		return this.numberOfNodes;
	}
	/*
	 * (non-Javadoc)
	 * @see graph.Graph#getTotalWeight()
	 */
	@Override
	public Float getTotalWeight() {
		return this.totalWeight;
	}

}
