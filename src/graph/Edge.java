package graph;

/**
 * Generic class that represents a weighted graph Edge, storing a 
 * generic data type D.
 * @param <D> Type of Data to be stored inside an Edge.
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class Edge<D> {
	/**
	 * Data in the Edge.
	 */
	private D data;
	/**
	 * Weight of the Edge.
	 */
	private Float weight;
	/**
	 * Node U of the Edge. 
	 */
	private Integer u;
	/**
	 * Node V of the Edge.
	 */
	private Integer v;
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((u == null) ? 0 : u.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) { 
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge<?> other = (Edge<?>) obj;
		if (v == null) {
			if (other.v != null)
				return false;
		} else if (!v.equals(other.v))
			return false;
		return true;
	}
	/**
	 * Constructor for the Edge Class.
	 * @param u node U
	 * @param v node V
	 * @param weight Weight of the edge.
	 * @param data Data to be stored in the edge.
	 */
	public Edge(Integer u, Integer v, Float weight, D data) {
		this.u=u;
		this.v=v;
		this.weight = weight;
		this.data = data;
}

	/**
	 * Getter for Node U.
	 * @return u node U
	 */
	public Integer getU() {
		return u;
	}
	/**
	 * Getter for Node V.
	 * @return V node V
	 */
	public Integer getV() {
		return v;
	}
	/**
	 * Getter for the Weight.
	 * @return weight
	 */
	public Float getWeight() {
		return weight;
	}
	/**
	 * Getter for the Data
	 * @return data
	 */
	public D getData() {
		return data;
	}
	/**
	 * Setter for the Data
	 */
	public void setData(D data) {
		this.data = data;
	}
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return u.toString();
	}
}
