package graph;

import java.util.LinkedList;

/**
 * Generic Graph Interface composed by Edges and Nodes.
 * Edges have at least one attribute named weight and there is
 * one special node called the origin node.
 * 
 * @param <N> Implementation of Node.
 * @param <E> Implementation of Edge.
 * 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public interface Graph<N,E> {

	/**
	 * Insert an edge in the Graph.
	 * @param edge
	 */
	public void insertEdge(E edge);
	
	/**
	 * Get the sum of the weight's of all
	 * the edges in the graph.
	 * @return Integer Sum of the weight's of all
	 * the edges in the graph.
	 */
	public Float getTotalWeight();
	
	/**
	 * Return the edges connected to a node.
	 * @param u Node of interest.
	 * @return LinkedList<E> List of edges connected to the provided node.
	 */
	public LinkedList<E> getEdges(N u);
	
	/**
	 * Returns the total number of nodes.
	 * @return Integer Total number of nodes.
	 */
	public Integer getNumberOfNodes();
	
	/**
	 * Returns the origin node.
	 * @return N Origin node.
	 */
	public N getOriginNode();
}
