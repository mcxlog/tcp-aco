package graph;

/**
 * Exception thrown when there is some problem with the graph structure that 
 * must stop the program execution. 
 * @author	Sebastiao Miranda, 67713. sebastiao.miranda@outlook.com
 * @author  Tomas Ferreirinha, 67725. tomas619@hotmail.com
 */
public class InvalidGraphException extends Exception {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for the InvalidGraphException Exception.
	 * @param string String to be passed to the super constructor.
	 */
	public InvalidGraphException(String string) {
		super(string);
	}
	
}